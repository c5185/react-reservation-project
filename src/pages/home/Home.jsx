import React from 'react';
import Featured from '../../components/featured/Featured';
import Header from '../../components/header/Header';
import Navbar from '../../components/navbar/Navbar';
import './home.css';
import PropertyList from './../../components/propertyList/PropertyList';
import MailLIst from './../../components/mailLIst/MailLIst';
import Footer from '../../components/footer/Footer';


const Home = () => {
  return (
    <>
      <div className='home'>
        <div className="container">
          <Navbar />
          <Header />
        </div>
      </div>

      <div className="home__container">
        <Featured />

        <h3 className='home__title'>Browe by  property type</h3>

        <PropertyList/>

        

        <h3 className='home__title'>home guest love</h3>
          <MailLIst/>


          
          <Footer/>
          
      </div>


    </>
  );
};

export default Home; 