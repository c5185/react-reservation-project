import React from 'react';

const ListSearchItem = () => {
    return (
        <>
            <div className="list__search">
                <h3>Searchh</h3>

                <div className="list__search__destination">
                    <p>destination</p>
                    <input type="text" name="" id="" placeholder='Destination' />
                </div>
                <div className="list__search__check_in_date">
                    <p>Check in date </p>
                    <input type="text" name="" value={'04/04/2022 to  09/04/2022'} placeholder='Destination' />
                </div>


                <div className="list__search__options">
                    <p>Options </p>
                    <div className="list__search__option">
                        <span>Min Price</span>
                        <input type="number" name="" id="" />
                    </div>
                    <div className="list__search__option">
                        <span>Max Price</span>
                        <input type="number" name="" id="" />
                    </div>

                    <div className="list__search__option">
                        <span>Adult</span>
                        <input type="number" name="" id="" />
                    </div>

                    <div className="list__search__option">
                        <span>Children</span>
                        <input type="number" name="" id="" />
                    </div>

                    <div className="list__search__option">
                        <span>Room</span>
                        <input type="number" name="" id="" />
                    </div>

                </div>


                <div className="">
                    <button className="btn">Search</button>
                </div>

            </div>

        </>
    );
};

export default ListSearchItem;