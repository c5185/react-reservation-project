import React from 'react';
import Footer from '../../components/footer/Footer';
import Header from '../../components/header/Header';
import Navbar from '../../components/navbar/Navbar';
import './list.css';
import ListDetailItem from './ListDetailItem';
import ListSearchItem from './ListSearchItem';

const photos = [
    {
      src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/261707778.jpg?k=56ba0babbcbbfeb3d3e911728831dcbc390ed2cb16c51d88159f82bf751d04c6&o=&hp=1",
    },
    {
      src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/261707367.jpg?k=cbacfdeb8404af56a1a94812575d96f6b80f6740fd491d02c6fc3912a16d8757&o=&hp=1",
    },
    {
      src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/261708745.jpg?k=1aae4678d645c63e0d90cdae8127b15f1e3232d4739bdf387a6578dc3b14bdfd&o=&hp=1",
    },
    {
      src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/261707776.jpg?k=054bb3e27c9e58d3bb1110349eb5e6e24dacd53fbb0316b9e2519b2bf3c520ae&o=&hp=1",
    },
    {
      src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/261708693.jpg?k=ea210b4fa329fe302eab55dd9818c0571afba2abd2225ca3a36457f9afa74e94&o=&hp=1",
    },
    {
      src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/261707389.jpg?k=52156673f9eb6d5d99d3eed9386491a0465ce6f3b995f005ac71abc192dd5827&o=&hp=1",
    },
  ];

const List = () => {
    return (
        <>
            <div className='home'>
                <div className="container">
                    <Navbar />
                    <Header type="list" />
                </div>



            </div>



            <div className="list__container">
                <div className="list__wrapper">
                    <ListSearchItem />
                </div>
                <div className="list__detail">
                <ListDetailItem
                  duration={500} img={photos[0].src}
                   title="america holiday" feature='King Room with Bath Tub - Disability Access' price={1200} bed={2} offer="free aireport texi" status='excelent' rating={4.2} /> 
 

<ListDetailItem
                  duration={500} img={photos[1].src}
                   title="mumbai mahanagar" feature='free one day ride' price={14500} bed={2} offer="free vip movie ticket" status='excelent' rating={4.2} /> 

<ListDetailItem
                  duration={1455} img={photos[2].src}
                   title="monsurie hill" feature='mind of peace' price={4500} bed={2} offer="free ride" status='excelent' rating={4.1} /> 
 

<ListDetailItem
                  duration={500} img={photos[3].src}
                   title="tower streeet apartment" feature=' Studio Apartment with Air conditioning' price={10000} bed={2} offer="free aireport texi and driver" status='excelent' rating={4.1} /> 

                   
                </div>
            </div>


 


        </>
    );
};

export default List;