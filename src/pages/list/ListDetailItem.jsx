import React from 'react';

const ListDetailItem = (props) => {


    let { title,img, feature, price, bed, offer,duration, status, rating } = props;


    return (
        <>
              <div className="list__detail__container">
                        <div className="list__box">
                            <div className="list__box__img">
                            <img
                                src={img}
                                alt=""
                                className="img"
                            />
                            </div>
                           <div className="list__right__detail">

                           <div className="list__box__detail">
                                <h3> {title} </h3>
                                <span>{status}</span>
                                <span className="rating">{rating}</span>

                            </div>

                            <div className="list__box__breadcrumb">
                                <span className="offer">{duration} meter from center</span>
                            </div>

                            <span className="extra">{offer}</span>
                            <p className='extra__Info'> {feature}</p>
                            <div className="extra__detail">
                                <span>{bed} bed</span>
                                <span className='price'>{price}</span>
                            </div>

                            <div className="info">
                                <span className="cancel">free cancellation </span>
                                <span className="tax">include all taxes</span>
                            </div>

                            <div className="after">
                                <span className="after__text">you casn cancel later. so look in the great price today</span>
                                <button className='btn' type='button'>see avalabilty</button>
                            </div>
                           </div>
                        </div>
                    </div>
                    
        </>
    );
};

export default ListDetailItem;