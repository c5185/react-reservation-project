import React from 'react';

const PropertyItem = ({img , title, subtitle }) => {
  return (
    <>
      <>
              <div className="pListItem">
          
          <img
              src={img}
              alt=""
              className="pListImg"
            />
            <div className="pListTitles">
              <h1>{title}</h1>
              <h2>{subtitle} </h2>
            </div>
          </div>

        </>
    </>
  );
};

export default PropertyItem;