import {
  faBed,
  faCalendar,
  faCar,
  faCocktail,
  faPerson,
  faPlane,
  faTaxi,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { DateRange } from "react-date-range";
import { format } from 'date-fns'
import "react-date-range/dist/styles.css";
import "react-date-range/dist/theme/default.css";
import "./header.css";


const Header = ({type}) => {


  const [openDate, setOpenDate] = useState(false);

  const [date, setDate] = useState([
    {
      startDate: new Date(),
      endDate: new Date(),
      key: "selection",
    },
  ]);

  const [options, setOptions] = useState({
    adult: 0,
    children: 0,
    rooms: 0,
  });
  const [openOptions, setOpenOptions] = useState(false);

  return (
    
    
    <div className={type === 'list' ?'header listMode':'header'}>


      <div className="headerList">
        <div className="headerListItem">
          <FontAwesomeIcon icon={faBed} />
          <p>stays</p>
        </div>

        <div className="headerListItem">
          <FontAwesomeIcon icon={faCocktail} />
          <p>attration</p>
        </div>

        <div className="headerListItem">
          <FontAwesomeIcon icon={faTaxi} />
          <p>airport texi</p>
        </div>

        <div className="headerListItem">
          <FontAwesomeIcon icon={faCar} />
          <p>carb renter</p>
        </div>

        <div className="headerListItem">
          <FontAwesomeIcon icon={faPlane} />
          <p>flight</p>
        </div>
      </div>


{  type !=='list' &&  <>  <h1 className="heading"> A lifetime of discounts? It's Genius.</h1>
      <p className="lead">
        Get rewarded for your travels - unlock instant savings of 10% or more
        with a free Lamabooking account
      </p>
      <div className="mt-1 mb-1">
        <button type="button" className="btn ">
          SignIn / Register
        </button>
      </div>

      <div className="header__search">
        <div className="header__search__item">
          <FontAwesomeIcon icon={faBed} className="icon" />
          <input
            type="text"
            placeholder="where are you going"
            className="input"
          />
        </div>

        <div className="header__search__item ">
          <FontAwesomeIcon icon={faCalendar} className="icon" />
          <span onClick={() => setOpenDate(!openDate)} >{`${format(date[0].startDate, 'MM/dd/yyyy')} To ${format(date[0].endDate, 'MM/dd/yyyy')}`} </span>

          {openDate && <DateRange className="date__range"
            editableDateInputs={true}
            onChange={item => setDate([item.selection])}
            moveRangeOnFirstSelection={false}
            ranges={date}
          />
          }
        </div>

        <div className="header__search__item">
          <FontAwesomeIcon icon={faPerson} className="icon" />
          <span onClick={()=>setOpenOptions(!openOptions)} >{options.adult} Adult {options.children} Children  {options.rooms} Room </span>

          {openOptions  && 

           <div className="options">


            <div className="options__items">
              <span className="options__text">Adult</span>
              <button className="options__btn" onClick={()=>setOptions( {...options,adult:options.adult+1  } )} >+</button>
              <span className="options__counter">{options.adult}</span>
              <button className="options__btn" >-</button>
            </div>

            <div className="options__items">
              <span className="options__text">Children</span>
              <button className="options__btn" onClick={()=>setOptions( {...options,children:options.children+1  } )} >+</button>
              <span className="options__counter">{options.children}</span>
              <button className="options__btn">-</button>
            </div>

            <div className="options__items">
              <span className="options__text">Rooms </span>
              <button className="options__btn"   onClick={()=>setOptions( {...options,rooms:options.rooms+1  } )}  >+</button>
              <span className="options__counter" >{options.rooms}</span>
              <button className="options__btn" >-</button>
            </div>

          </div>
}



        </div>

        <div className="header__search__item">
          <button type="button" className=" search__btn">
            Search
          </button>
        </div>
      </div>
      </>
}
    </div>
  );
};

export default Header;
