import "./navbar.css";


const Navbar = () => {
    return (
       <nav className='navbar'>
           <div className="navbar__container">
               <span className="logo">booking system</span>
             <div className="navbar__items">
                 <button className="btn navbar__item__btn" type='button'>login</button>
                 <button className="btn navbar__item__btn" type='button'>register</button>
             </div>
           </div>
       </nav>
    );
};

export default Navbar;;